import {Routes, RouterModule} from '@angular/router';
import {OfficeComponent} from './app/office/office.component';
import {MapComponent} from './app/map/map.component';
import {HomeComponent} from './app/home/home.component';
import {CaseFilesComponent} from './app/case-files/case-files.component';

const routes: Routes = [
  {
    path: 'office',
    pathMatch: 'full',
    component: OfficeComponent
  },
  {
    path: 'map',
    pathMatch: 'full',
    component: MapComponent
  },
  {
    path: 'caseFiles',
    pathMatch: 'full',
    component: CaseFilesComponent
  },
  {
    path: '**',
    component: HomeComponent
  }
  ];

export const appRoutingModule = RouterModule.forRoot(routes);
