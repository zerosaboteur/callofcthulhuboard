import { Component, OnInit } from '@angular/core';
import * as artifacts from '../../resources/artifacts.json';
@Component({
  selector: 'app-office',
  templateUrl: './office.component.html',
  styleUrls: ['./office.component.css']
})
export class OfficeComponent implements OnInit {

  artifacts = artifacts.items;
  constructor() { }

  ngOnInit(): void {
  }

}
