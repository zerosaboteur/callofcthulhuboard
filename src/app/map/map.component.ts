import { Component, OnInit } from '@angular/core';
import * as Leaflet from 'leaflet';
import * as Events from '../../resources/events.json';

// setting base marker, because of Angular Leaflet bug
const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = Leaflet.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
Leaflet.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  private map;

  constructor() { }

  ngOnInit(): void {
    this.initMap();
  }

  private initMap(): void {
    this.map = Leaflet.map('map', {
      center: [ 39.8282, -98.5795 ],
      zoom: 3
    });

    const tiles = Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);

    this.CreateMarkers();

    // added simple function to get coordinates on click
    this.map.on('click', e => {
      const coord = e.latlng;
      const lat = coord.lat;
      const lng = coord.lng;
      console.log('You clicked the map at latitude: ' + lat + ' and longitude: ' + lng);
    });
  }

  // Create markers on map
  private CreateMarkers(): void {
    Events.data.forEach((event) => {
      // @ts-ignore
      const marker = Leaflet.marker(event.coordinates).addTo(this.map);
      marker.bindPopup('<b>' + event.title + '</b><h6>' + event.date + '</h6>' + event.description);
    });
  }

}
