import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OfficeComponent } from './office/office.component';
import { MapComponent } from './map/map.component';
import { appRoutingModule} from '../app.routing';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {PinchZoomModule} from 'ngx-pinch-zoom';
import {MatCardModule} from '@angular/material/card';
import { HomeComponent } from './home/home.component';
import { NoteComponent } from '../components/note/note.component';
import { CaseFilesComponent } from './case-files/case-files.component';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  declarations: [
    AppComponent,
    OfficeComponent,
    MapComponent,
    HomeComponent,
    NoteComponent,
    CaseFilesComponent
  ],
  imports: [
    BrowserModule,
    appRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    PinchZoomModule,
    MatCardModule,
    MatTableModule,
    MatSortModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
